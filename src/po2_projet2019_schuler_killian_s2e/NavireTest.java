package po2_projet2019_schuler_killian_s2e;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

public class NavireTest {
	
	@Test
	public void testSetPosHor() {
		Navire n=new Navire(2);
		n.setPos(0, 0, 0);
		ArrayList<Case> c=n.getPosition();
		assertEquals(c.get(0), new Case(0, 0));
		assertEquals(c.get(1), new Case(1, 0));
	}
	
	@Test
	public void testSetPosVer() {
		Navire n=new Navire(2);
		n.setPos(0, 0, 1);
		ArrayList<Case> c=n.getPosition();
		assertEquals(c.get(0), new Case(0, 0));
		assertEquals(c.get(1), new Case(0, 1));
	}

	@Test
	public void testToucher() {
		//preparation
		Navire n=new Navire(2);
		n.setPos(0, 0, 0);
		//test 1 touche
		int i=n.toucher(0, 0);
		assertEquals(i, 1);
		assertEquals(n.getVie(), 1);
		//test touche au meme endroit
		i=n.toucher(0, 0);
		assertEquals(i, 0);
		assertEquals(n.getVie(), 1);
		//test touche coule
		i=n.toucher(1, 0);
		assertEquals(i, 2);
		assertEquals(n.getVie(), 0);
		assertEquals(n.getCoule(), true);
		//test touche endroit inexistant
		i=n.toucher(0, 1);
		assertEquals(i, 0);
	}
	
	@Test(expected = BateauException.class)
	public void testFreeCollision() throws BateauException {
		Navire n=new Navire(5);
		n.setPos(0, 0, 0);
		ArrayList<Case> c=new ArrayList<Case>();
		c.add(new Case(0,0));
		n.CheckIfFree(c);
	}
	
	public void testFreeNoCollision() {
		Navire n=new Navire(5);
		n.setPos(0, 0, 0);
		ArrayList<Case> c=new ArrayList<Case>();
		c.add(new Case(6,0));
	}

}
