package po2_projet2019_schuler_killian_s2e;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Scanner;

public class Jeu implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * les deux joueur d'une partie
	 */
	private Joueur j1, j2;
	
	
	/**
	 * methode chargee de lancer une partie (selection de la grille et placement des navires)
	 * @throws BateauException exception levee si il y a un probleme de placement des navires
	 */
	public void debut() throws BateauException {
		Scanner sc=new Scanner(System.in);
		this.choixGrille(sc);
		this.placementBateau(this.j1, 5,sc);
		this.placementBateau(this.j1, 4,sc);
		this.placementBateau(this.j1, 3,sc);
		this.placementBateau(this.j1, 3,sc);
		this.placementBateau(this.j1, 2,sc);
		this.placementBateau(this.j2, 5,sc);
		this.placementBateau(this.j2, 4,sc);
		this.placementBateau(this.j2, 3,sc);
		this.placementBateau(this.j2, 3,sc);
		this.placementBateau(this.j2, 2,sc);
		sc.close();
	}
	
	/**
	 * methode chargee de creer les grilles utilisees lors du jeu
	 * @param sc scanner utilise pour la saisie utilisateur
	 */
	public void choixGrille(Scanner sc) {
		System.out.println("Choisissez la taille de la grille :\n");
		System.out.println("Longueur=");
		int a=sc.nextInt();
		System.out.println("Hauteur=");
		int b=sc.nextInt();
		this.j1=new Joueur(a, b, 1);
		this.j2=new Joueur(a, b, 2);
		j1.setJE(j2);
		j2.setJE(j1);
	}
	
	/**
	 * methode chargee de placer un navire sur la grille d'un joueur
	 * @param j joueur qui place le navire
	 * @param t taille du navire a placer
	 * @param sc scanner utilise pour la saisie utilisateur
	 * @throws BateauException exception levee si il y a un probleme de placement du navire (sortie de grille ou bateau deja present
	 */
	public void placementBateau(Joueur j, int t, Scanner sc) throws BateauException {
		System.out.println("Placement des navires du joueur "+j.getId()+" :\n");
		System.out.println("Placement du navire ("+t+" cases) :\n");
		System.out.println("x=");
		int x = sc.nextInt();
		System.out.println("y=");
		int y = sc.nextInt();
		System.out.println("Sens= (0=horizontal, 1=vertical)");
		int s=sc.nextInt();
		Navire n=new Navire(5);
		n.setPos(x,y,s);
		j1.setJE(j2);
		j2.setJE(j1);
	}
	
	/**
	 * getter de j1
	 * @return j1
	 */
	public Joueur getJ1() {
		return this.j1;
	}
	
	/**
	 * getter de j2
	 * @return j2
	 */
	public Joueur getJ2() {
		return this.j2;
	}
	
	/**
	 * methode chargee de simuler un tour de jeu
	 * @param j joueur dont c'est le tour
	 * @param x coordonnees horizontale de l'attaque
	 * @param y coordonnees verticale de l'attaque
	 * @throws BateauException exception levee si le joueur tir en dehors de la grille
	 */
	public void tour(Joueur j, int x, int y) throws BateauException {
		int i=j.attaquer(x, y);
		switch (i) {
		case 0 : System.out.println("Vous avez frappe une case deja touchee");
		break;
		case 1 : System.out.println("Vous avez touche une case vide");
		break;
		case 2 : System.out.println("Vous avez touche un navire");
		break;
		case 3 : System.out.println("Vous avez coule un navire");
		break;
		}
	}
	
	/**
	 * methode pour charger une partie en cours depuis un fichier
	 * @param src fichier ou la partie est enregistree
	 * @throws FileNotFoundException le fichier n'est pas trouvable
	 * @throws IOException erreur d'entree/sortie
	 * @throws ClassNotFoundException erreur d'enregistrement du jeu dans l'etrange cas ou cette classe n'existe pas
	 */
	public static Jeu load(String src) throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream ois=new ObjectInputStream(new FileInputStream(src));
		Jeu j=(Jeu)ois.readObject();
		ois.close();
		return j;
	}
	
	/**
	 * methode chargee d'enregistrer la partie en cours dans un fichier
	 * @param dest fichier ou enregistrer la partie
	 * @throws FileNotFoundException le fichier n'est pas trouvable
	 * @throws IOException erreur d'entree/sortie
	 */
	public void save(String dest) throws FileNotFoundException, IOException {
		ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(dest));
		oos.writeObject(this);
		oos.close();
	}

}
