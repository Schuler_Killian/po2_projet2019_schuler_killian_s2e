package po2_projet2019_schuler_killian_s2e;

import java.io.Serializable;
import java.util.ArrayList;

public class Grille implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * hauteur de la grille
	 */
	private int hauteur;
	
	/**
	 * longueur de la grille
	 */
	private int longueur;
	
	/**
	 * liste des cases de la grille
	 */
	private ArrayList<Case> cases;
	
	/**
	 * liste des navires poses sur la grille
	 */
	private ArrayList<Navire> f;
	
	/**
	 * constructeur chargee d'initialiser les attributs de la grille
	 * @param x longueur de la grille
	 * @param y hauteur de la grille
	 */
	public Grille(int x, int y) {
		this.hauteur=y;
		this.longueur=x;
		this.f=new ArrayList<Navire>();
		this.cases=new ArrayList<Case>();
		for (int i=0;i<x;i++) {
			for (int j=0;j<y;j++) {
				this.cases.add(new Case(i, j));
			}
		}
	}
	
	/**
	 * methode chargee de verifier si la flotte peut etre posee sur la grille
	 * @param n flotte a poser sur la grille
	 * @throws BateauException exception si les navires sortent de la grille
	 */
	public void setF(ArrayList<Navire> n) throws BateauException {
		for (Navire nv : n) {
			this.checkIfFit(nv.getPosition());
		}
		this.f=n;
	}
	
	/**
	 * getter de f
	 * @return
	 */
	public ArrayList<Navire> getF() {
		return this.f;
	}
	
	/**
	 * methode chargee de renvoyer la case de la grille aux coordonnees indiquees
	 * @param x coordonnee horizontale
	 * @param y coordonnee verticale
	 * @return case a la position x,y
	 */
	public Case getCase(int x, int y) {
		Case res=null;
		for (Case c : this.cases) {
			if (c.getX()==x && c.getY()==y) {
				res=c;
			}
		}
		return res;
	}
	
	/**
	 * methode chargee de simuler un tir sur une case de la grille
	 * @param x coordonnee horizontale de l'attaque
	 * @param y coordonnee verticale de l'attaque
	 * @return un entier indiquant le resultat (0=rate, 1=touche, 2=coule)
	 * @throws BateauException exception levee si le tir est en dehors de la grille
	 */
	public int tir(int x, int y) throws BateauException {
		int t=0;
		int i=this.cases.indexOf(new Case(x,y));
		if (i!=-1) {
			if (this.cases.get(i).getTouche()==false) {
				t=1;
				this.cases.get(i).toucher();
				for (int j=0;j<this.f.size();j++) {
					int k=this.f.get(j).getPosition().indexOf(new Case(x,y));
					if (k!=-1) {
						t=2;
						this.f.get(j).toucher(x, y);
						if (this.f.get(j).getCoule()) {
							t=3;
						}
					}
				}
			}
		} else {
			throw new BateauException("Vous avez tire en dehors de la grille !");
		}
		return t;
	}
	
	/**
	 * methode chargee de verifier si le navire est contenu dans les limites de la grille
	 * @param test liste des cases positions du navire a tester
	 * @throws BateauException exception levee si la navire sort de la grille ou qu'un navire occupe deja cette position
	 */
	public void checkIfFit(ArrayList<Case> test) throws BateauException {
		for (Case c : test) {
			if(!(c.getX()<this.longueur && c.getY()<this.hauteur)) {
				throw new BateauException("Le navire sort des limites de la grille !");
			}
			for (Navire n : this.f) {
				n.CheckIfFree(test);
			}
		}
	}

}
