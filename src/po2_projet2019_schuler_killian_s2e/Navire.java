package po2_projet2019_schuler_killian_s2e;

import java.io.Serializable;
import java.util.ArrayList;

public class Navire implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * taille du bateau
	 */
	private int taille;
	
	/**
	 * vie du bateau, coule si elle atteint 0
	 */
	private int vie;
	
	/**
	 * liste des cases ou le bateau est place
	 */
	private ArrayList<Case> position;
	
	/**
	 * booleen true si le navire a coule et false sinon
	 */
	private boolean coule;
	
	/**
	 * constructeur chargee d'initialiser les attributs
	 * @param i taille (et donc vie) du navire
	 */
	public Navire(int i) {
		this.coule=false;
		this.taille=i;
		this.vie=i;
		this.position=new ArrayList<Case>();
	}
	
	/**
	 * methode chargee de placer le navire
	 * @param x coordonnee horizontale
	 * @param y coordonnee verticale
	 * @param s sens du bateau, le navire sera placer la tete aux coordonnees indiquees (le reste decale vers la droite ou vers le bas selon le sens)
	 */
	public void setPos(int x, int y, int s) {
		if (s==0) {
			for (int i=0;i<this.vie;i++) {
				this.position.add(new Case(x, y));
				x++;
			}
		} else {
			for (int i=0;i<this.vie;i++) {
				this.position.add(new Case(x, y));
				y++;
			}
		}
	}
	
	/**
	 *  getter de position
	 * @return position
	 */
	public ArrayList<Case> getPosition() {
		return this.position;
	}
	
	/**
	 * methode chargee de simuler un tir sur ce bateau (et d'indiquer le resultat)
	 * @param x coordonnee horizontale du tir
	 * @param y coordonnee verticale a attaquer
	 * @return entier indiquant le resultat du tir (0=rate, 1=touche, 2= coule)
	 */
	public int toucher(int x, int y) {
		int t=0;
		int i=this.position.indexOf(new Case(x, y));
		if (i!=-1) {
			if (this.position.get(i).getTouche()==false) {
				t=1;
				this.position.get(i).toucher();
				this.vie--;
				if (this.vie == 0) {
					t = 2;
					this.coule = true;
				} 
			} 
		}
		return t;
	}
	
	/**
	 * getter de coule
	 * @return coule
	 */
	public boolean getCoule() {
		return this.coule;
	}
	
	/**
	 * methode chargee de verifier si des cases sont occupees par ce navire
	 * @param n cases a verifier
	 * @throws BateauException exception envoyer si les cases sont deja occupees par ce navire
	 */
	public void CheckIfFree(ArrayList<Case> n) throws BateauException {
		for (Case c : n) {
			for (Case d : this.getPosition()) {
				if (c.equals(d)) {
					throw new BateauException("Un bateau occupe deja cette position !");
				}
			}
		}
	}
	
	public String toString() {
		String res=new String();
		if (this.coule) {
			res="Bateau de taille "+this.taille+" coule";
		} else {
			if (this.taille==this.vie) {
				res="Bateau de taille"+this.taille+" intact\n Position :\n";
				for (Case c : this.position) {
					res+=c.toString()+"\n";
				}
			} else {
				res="Bateau de taille"+this.taille+"\n Position :\n";
				for (Case c : this.position) {
					res+=c.toString();
					if (c.getTouche()) {
						res+=" touche";
					}
					res+="\n";
					}		
			}
		}
		return res;
	}
	
	/**
	 * getter de la vie du bateau
	 * @return vie du joueur
	 */
	public int getVie() {
		return this.vie;
	}

}
