package po2_projet2019_schuler_killian_s2e;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws BateauException, FileNotFoundException, ClassNotFoundException, IOException {
		Jeu j=new Jeu();
		Scanner sc=new Scanner(System.in);
		System.out.println("Souhaitez-vous charger une partie (1=Oui, 2=Non) ? :");
		int rep=sc.nextInt();
		if (rep==1) {
			System.out.println(" :");
			String s=sc.next();
			j=Jeu.load(s);
		} else {
			j.debut();
			j.save("test");
		}
		int c=1;
		while (c!=2) {
			System.out.println("Joueur 1 : Entrez les coordonnees cibles :\n");
			System.out.println("x=");
			int x = sc.nextInt();
			System.out.println("y=");
			int y = sc.nextInt();
			j.tour(j.getJ1(), x, y);
			System.out.println("Souhaitez-vous continuer (1=Oui, 2=Non) ? :");
			c=sc.nextInt();
			if (c!=2) {
				System.out.println("Joueur 2 : Entrez les coordonnees cibles :\n");
				System.out.println("x=");
				x = sc.nextInt();
				System.out.println("y=");
				y = sc.nextInt();
				j.tour(j.getJ2(),x ,y);
				System.out.println("Souhaitez-vous continuer (1=Oui, 2=Non) ? :");
				c=sc.nextInt();
			}
		}
		System.out.println("Souhaitez-vous sauvegarder la partie (1=Oui, 2=Non) ? :");
		rep=sc.nextInt();
		if (rep==1) {
			System.out.println("Entrez le chemin du fichier ou sauvegarder :");
			String s=sc.next();
			j.save(s);
		}
	}

}
