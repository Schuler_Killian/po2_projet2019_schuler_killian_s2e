package po2_projet2019_schuler_killian_s2e;

public class BateauException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public BateauException(String msg) {
		super(msg);
	}

}
