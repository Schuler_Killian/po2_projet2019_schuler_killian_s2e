package po2_projet2019_schuler_killian_s2e;

import java.io.Serializable;

public class Case implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private int x, y;
	private boolean touche;
	
	public Case(int a, int b) {
		this.touche=false;
		this.x=a;
		this.y=b;
	}
	
	public int getX() {
		return this.x;
	}
	public int getY() {
		return this.y;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean res=false;
		if (o instanceof Case) {
			Case c=(Case)o;
			res=(c.getX()==this.x)&&(c.getY()==this.y);
		}
		return res;
	}
	
	public boolean getTouche() {
		return this.touche;
	}
	
	public void toucher() {
		this.touche=true;
	}
	
	public String toString() {
		String res="x="+this.x+", y="+this.y;
		return res;
	}

}
