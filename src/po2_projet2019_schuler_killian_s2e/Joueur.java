package po2_projet2019_schuler_killian_s2e;

import java.io.Serializable;
import java.util.ArrayList;

public class Joueur implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * entier designant l'id du joueur
	 */
	private int id;
	
	/**
	 * grille du joueur
	 */
	private Grille g;
	
	/**
	 * joueur ennemi
	 */
	private Joueur je;
	
	/**
	 * flotte du joueur
	 */
	private ArrayList<Navire> flotte;
	
	/**
	 * constructeur charge d'initialiser les differents attributs
	 * @param a taille horizontale de la grille
	 * @param b taille verticale de la grille
	 * @param i id du joueur
	 */
	public Joueur(int a, int b, int i) {
		this.id=i;
		this.g=new Grille(a, b);
		this.flotte=new ArrayList<Navire>();
	}
	
	/**
	 * methode chargee de placer un navire dans la flotte du joueur
	 * @param n navire a placer sur la grille
	 * @return booleen indiquant si le placement s'est bien fait (true c'est le cas)
	 * @throws BateauException
	 */
	public boolean placement(Navire n) throws BateauException {
		boolean b=false;
		this.flotte.add(n);
		this.g.setF(this.flotte);
		return b;
	}
	
	/**
	 * methode chargee d'attaquer une case de la grille ennemi
	 * @param x coordonnee horizontale a attaquer
	 * @param y coordonnee verticale a attaquer
	 * @return un entier indiquant le resultat (0=rate, 1=touche, 2=coule)
	 * @throws BateauException
	 */
	public int attaquer(int x, int y) throws BateauException {
		return this.je.getGrille().tir(x, y);
	}
	
	/**
	 * getter de l'id
	 * @return id du joueur
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * getter de la grille
	 * @return grille du joueur
	 */
	public Grille getGrille() {
		return this.g;
	}
	
	/**
	 * getter de la grille ennemi
	 * @return grille du joueur ennemi
	 */
	public Joueur getJE() {
		return this.je;
	}
	
	/**
	 * setter de l'ennemi
	 * @param jr joueur ennemie
	 */
	public void setJE(Joueur jr) {
		this.je=jr;
	}
	
	/**
	 * methode chargee d'indiquer si le joueur a perdu
	 * @return booleen true si le joueur n'a plus de navire et a donc perdu
	 */
	public boolean perdre() {
		boolean b=true;
		for (Navire n : this.g.getF()) {
			if (!n.getCoule()) {
				b=false;
			}
		}
		return b;
	}
}
